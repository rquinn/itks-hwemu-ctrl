# itks-hwemu-ctrl

A collection of scripts for controlling the FELIG-based ITk Strips emulator found at the [phase2/FLX-1592_feligFLX712_ITkStripsEmu](https://gitlab.cern.ch/atlas-tdaq-felix/firmware/-/tree/phase2/FLX-1592_feligFLX712_ITkStripsEmu) branch of [atlas-tdaq-felix/firmware](https://gitlab.cern.ch/atlas-tdaq-felix/firmware).

## Introduction
The scripts in this package provide an interface to emulator memory blocks as well as functions to generate data patterns in the correct format. Access to the FELIG card requires a copy of [FELIX software](https://gitlab.cern.ch/atlas-tdaq-felix/felix-distribution/) to be installed, and actually using the emulator requires [YARR](https://gitlab.cern.ch/YARR/YARR) and/or [ezhivun/hcc-star-python-readout](https://gitlab.cern.ch/ezhivun/hcc-star-python-readout) (or your own shell scripts).

The scripts also require the `bitarray` module, which in turn requires `python3-devel` on CC7. It is convenient to create a virtual environment from `requirements.txt` into the `env/` folder:
```bash
yum install python3 python3-devel
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```
as `env/` is ignored by `.gitignore`.

## Usage
The instructions on this page assume the following hardware structure:
 - FELIX card 0: FLX-712 card with strips FELIX firmware installed
 - FELIX card 1: FLX-712 card with FELIG strips emulator firmware installed

and that you've already set up a phase-II FELIX software distribution (see [atlas-tdaq-felix/felix-distribution](https://gitlab.cern.ch/atlas-tdaq-felix/felix-distribution/)) and activated a virtual environment with `bitarray`. 

### Setup
We must first configure the cards. Initialize both so their links align:
```bash
flx-init -c 0 # felix
flx-init -c 1 # felig
```
It's useful to wait a while before checking to make sure the expected links are aligned:
```bash
sleep 10
flx-info -c 0 link # felix
flx-info -c 1 link # felig
```

Once links are aligned, we can configure the FELIX e-links. The following script is a modified version of the elink enable script from [ezhivun/hcc-star-python-readout](https://gitlab.cern.ch/ezhivun/hcc-star-python-readout):
```bash
source utils/enable_elinks.sh
```
which sets 8-bit elink widths and ensures 8b10b decoding for four lpGBT lanes.

We next configure the emulator to use all 8b10b-encoded 8-bit elinks using `felig-config`:
```bash
felig-config -d 2 \
	--endian_mode=0x00 \
	--data_format=0x1555 \
	--elink_input_width=0x7f \
	--elink_output_width=0x92492 \
	--elink_enable=0xfffffff \
	--b_channel_select=0xff
```
At this point, the emulator will respond to register reads and writes, but we will still need to load packet data for it to respond to L0A signals.

### Loading Data
This repository handles the generation and loading of packet data. There are two options: use `main.py` with presaved data (`data/defaults/` contains pregenerated data patterns), or write your own python script. `utils/example.py` provides examples of how to generate and load data using the `memctl` and `datagen` libraries, and `main.py` is fairly simple:
```bash
./main.py -f data/defaults/dizzy_16.txt -d 2 -g 0
```
