#!/usr/bin/env python3
"""main.py

Basic script to load data from a file into HCCStar emulator memory.

Todo
----
add option to use yaml config files for per-abc configs
"""

import datagen
import memctl

import argparse
from bitarray.util import urandom
import logging

logger = logging.getLogger(__name__)


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description='Write ABCStar event data to HCCStar emulator memory.'
    )
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        '-f', '--file',
        type=str, help='file containing packet data')
    group.add_argument(
        '-r', '--random',
        action='store_true', help='generate random data')
    parser.add_argument(
        '-d', '--flx-device',
        type=int, required=True, help='FELIX device')
    parser.add_argument(
        '-g', '--gbt-lane',
        choices=list(range(24)), required=True, help='lpGBT lane')
    parser.add_argument(
        '-h', '--hcc-index',
        choices=list(range(14)), required=True, help='HCC index')
    parser.add_argument(
        '-l', '--log-level',
        choices=['warn', 'info', 'debug'], default='warn', help='log level')
    return parser


def main():
    args = create_parser().parse_args()

    log_level = logging.WARNING
    if args.log_level == 'info':
        log_level = logging.INFO
    elif args.log_level == 'debug':
        log_level = logging.DEBUG
    logging.basicConfig(level=log_level)

    mem_id = (args.flx_device, args.gbt_lane, args.hcc_index)

    if args.random:
        abc_masks = {}
        for abcid in range(10):
            abc_masks[abcid] = urandom(256)
        packet = datagen.packet_from_masks(abc_masks)
    else:
        packet = datagen.load_packet(args.file)

    memctl.reset(mem_id)
    memctl.load(mem_id, packet)


if __name__ == '__main__':
    main()
