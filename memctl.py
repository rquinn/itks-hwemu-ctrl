"""memctl.py

Functions used to interact with FELIG ITk Strips emulator memory blocks.
"""

from typing import Tuple, List, Union
from bitarray import bitarray
from bitarray.util import int2ba, ba2int
import subprocess
from tqdm import tqdm
import logging
logger = logging.getLogger(__name__)


def _control_register_prefix(link: int, hcc: int) -> str:
    """Prefix of data gen control registers
    """
    return f'FELIG_ITK_STRIPS_DATA_GEN_CONFIG_LINK{link:02d}_HCC{hcc:02d}'


def _monitor_register_prefix(link: int, hcc: int) -> str:
    """Prefix of data gen monitor registers
    """
    return f'FELIG_MON_ITK_STRIPS_LINK{link:02d}_HCC{hcc:02d}'


def register_memory_write_enable(link: int, hcc: int) -> str:
    """Register name for memory read enable
    """
    return f'{_control_register_prefix(link, hcc)}_MEMORY_WRITE_ENABLE'


def register_memory_data_in(link: int, hcc: int) -> str:
    """Register name for memory data input lines
    """
    return f'{_control_register_prefix(link, hcc)}_MEMORY_DATA_IN'


def register_memory_reset(link: int, hcc: int) -> str:
    """Register name for memory data input lines
    """
    return f'{_control_register_prefix(link, hcc)}_MEMORY_RESET'


def register_memory_write_acknowledge(link: int, hcc: int) -> str:
    """Register name for memory write acknowledge
    """
    return f'{_monitor_register_prefix(link, hcc)}_MEMORY_WRITE_ACKNOWLEDGE'


def _flx_config(flx_dev: int, register: str, value: int = None) \
        -> subprocess.CompletedProcess:
    """Gets or sets a register's value via `flx-config`

    If `value` is supplied, the register is written, otherwise the register
    is read.

    Parameters
    ----------
    flx_dev : int
        FELIX device
    register : str
        Register to access
    value : int, default=None
        Value to write

    Returns
    -------
    subprocess.CompletedProcess
        Result of calling subprocess.run()
    """
    cmd = ['flx-config', '-d', str(flx_dev)]
    if value is not None:
        cmd += ['set', f'{register}=0x{value:x}']
    else:
        cmd += ['get', f'{register}']
    try:
        logging.debug(' '.join(cmd))
        return subprocess.run(cmd, encoding='utf-8',
                              stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                              check=True)
    except subprocess.CalledProcessError as err:
        logging.error(f'Command`{" ".join(err.cmd)}` failed with output')
        for line in err.output.split('\n'):
            if line:
                logging.error(line)
        raise SystemExit
    except FileNotFoundError:
        logging.critical('flx-config not found. Set up FELIX software first.')
        raise SystemExit


def _set_flx_reg(flx_dev: int, register: str, value: int) -> None:
    """Sets a register's value via `flx-config`

    Parameters
    ----------
    register : str
        Register to write to
    value : int
        Value to be written
    """
    _flx_config(flx_dev, register, value=value)


def _get_flx_reg(flx_dev: int, register: str) -> str:
    """Gets a register's value via `flx-config`

    Parameters
    ----------
    flx_dev : int
        FELIX device
    register : str
        Register to read from

    Returns
    -------
    str
        Register value as output by `flx-config`
    """
    res = _flx_config(flx_dev, register).stdout.rstrip('\n')
    logging.debug(res)
    return res


def load_one(mem_id: Tuple[int, int, int], word: Union[bitarray, int],
             add_last: bool = False) -> None:
    """Writes a single 17-bit value into memory

    Only the 17 rightmost bits of `word` are used. If necessary, a `1` bit
    is prepended to the value to indicate that this is the last word to be
    read out in an event.

    Parameters
    ----------
    mem_id : tuple[int, int, int]
        FELIX device, lpGBT lane, and HCC index to identify memory block
    word : Union[bitarray, int]
        Value to write to memory
    add_last : bool, default=False
        Indicates if the MSB should be set

    Raises
    ------
    RuntimeError
        If memory fails to acknowledge write
    """
    dev, link, hcc = mem_id

    if isinstance(word, int):
        word = int2ba(word, length=17, endian='big')
    word[-17] |= add_last
    logging.info(f'Writing {ba2int(word):#07x} to memory {mem_id}')

    _set_flx_reg(dev, register_memory_write_enable(link, hcc), 0)
    _set_flx_reg(dev, register_memory_data_in(link, hcc), ba2int(word[-17:]))
    _set_flx_reg(dev, register_memory_write_enable(link, hcc), 1)

    # check write acknowledge before clearing write enable
    try:
        raw_status = _get_flx_reg(
            dev, register_memory_write_acknowledge(link, hcc))

        dump = f'{register_memory_write_acknowledge(link, hcc)}=0x'
        status = int(raw_status.replace(dump, '', 1), 16)

        if not status:
            msg = f'Device {dev} lpGBT lane {link} HCC {hcc}' \
                + ' did not acknowledge write.'
            logging.error(msg)
            raise SystemExit

    finally:
        _set_flx_reg(dev, register_memory_write_enable(link, hcc), 0)


def load(mem_id: Tuple[int, int, int], words: Union[List[bitarray], List[int]],
         needs_last: bool = False) -> None:
    """Writes multiple 17-bit words into memory

    Parameters
    ----------
    mem_id : tuple[int, int, int]
        FELIX device, lpGBT lane, and HCC index to identify memory block
    words : Union[List[bitarray], List[int]]
        Words to be written to memory
    needs_last : bool, default=False
        Indicates if the function should manually add a last-word flag to
        the last word in the list

    Raises
    ------
    RuntimeError
        If memory fails to acknowledge a write
    """
    for widx, word in enumerate(tqdm(words)):
        add_last = needs_last and (widx == len(words) - 1)
        load_one(mem_id, word, add_last=add_last)


def reset(mem_id: Tuple[int, int]) -> None:
    """Reset (clear) memory block

    Parameters
    ----------
    mem_id : Tuple[int, int, int]
        FELIX device, lpGBT lane, and HCC index to identify memory block
    """
    logging.info(f'Resetting memory {mem_id}')
    dev, link, hcc = mem_id
    _set_flx_reg(dev, register_memory_reset(link, hcc), 0)
    _set_flx_reg(dev, register_memory_reset(link, hcc), 1)
    _set_flx_reg(dev, register_memory_reset(link, hcc), 0)
