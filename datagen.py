"""datagen.py

Functions used to generate cluster data in the form of ABCStar physics packets.
"""

from typing import Dict, List
from bitarray import bitarray
from bitarray.util import int2ba, hex2ba, zeros, ba2int


def _resize_left(data: bitarray, size: int) -> bitarray:
    """Resizes `data` to `size` bits by padding zeros or removing bits on the
    left.

    Parameters
    ----------
    data : bitarray
        Input data
    size : int
        Desired output length in bits

    Returns
    -------
    bitarray
        Resized data
    """
    padding = size - len(data)
    if padding > 0:
        pdata = zeros(size)
        pdata[padding:] = data
        return pdata
    return data[-size:]


def _resize_right(data: bitarray, size: int) -> bitarray:
    """Resizes `data` to `size` bits by padding zeros or removing bits on the
    right.

    Parameters
    ----------
    data : bitarray
        Input data
    size : int
        Desired output length in bits

    Returns
    -------
    bitarray
        Resized data
    """
    padding = size - len(data)
    if padding > 0:
        pdata = zeros(size)
        pdata[:len(data)] = data
        return pdata
    return data[:size]


def _create_cluster(hit_addr: int, next_strips: bitarray) -> bitarray:
    """Creates 12-bit cluster bitarray.

    Least significant bit is used to indicate if this is the last cluster in a
    packet. We throw away this bit as soon as the packet gets to the HCCStar,
    so it's left as zero here.

    Parameters
    ----------
    hit_addr : int
        Address of hit
    next_hits : bitarray
        Hits from next 3 strips (MSB is nearest neighbor). Right-padded if
        length is less than 3.

    Returns
    -------
    bitarray
        12-bit cluster.
    """
    cluster = zeros(12)
    cluster[:8] = int2ba(hit_addr, length=8, endian='big')
    cluster[8:11] = _resize_right(next_strips, 3)
    return cluster


def _find_clusters(mask: bitarray) -> List:
    """Creates List of 12-bit cluster bitarrays.

    Mimics the behavior of an ABCStar cluster finder.

    Parameters
    ----------
    mask : bitarray
        256-bit strip masks

    Returns
    -------
    List of bitarray
        List of 12-bit clusters
    """
    # split into upper and lower rows
    d0 = mask[128:]
    d1 = mask[:128]

    # reverse to make indexing easier
    d0.reverse()
    d1.reverse()

    clusters = []
    while (d0 != zeros(128)) or (d1 != zeros(128)):
        if d1 != zeros(128):
            hit_addr = d1.index(1)
            next_strips = d1[hit_addr+1:hit_addr+4]
            clusters.append(_create_cluster(hit_addr+128, next_strips))
            d1[hit_addr:hit_addr+4] = 0

        if len(clusters) > 63:
            break

        if d0 != zeros(128):
            hit_addr = d0.index(1)
            next_strips = d0[hit_addr+1:hit_addr+4]
            clusters.append(_create_cluster(hit_addr, next_strips))
            d0[hit_addr:hit_addr+4] = 0

        if len(clusters) > 63:
            break

    return clusters


#
# public methods
#


def packet_from_masks(masks: Dict[int, bitarray]) -> List[bitarray]:
    """Create partial HCC packet with ABC data from strip masks

    Emulates ABCStar chip data in Static Test mode, where strip masks are used
    as hits. The cluster words are formatted as they would be output by the
    receiving HCCStar (6-bit ABCID, 11-bit cluster data with LSB removed).

    Parameters
    ----------
    masks : Dict[int, bitarray]
        ABCIDs and their corresponding 256-bit strip masks

    Returns
    -------
    List[bitarray]
        Packet words
    """
    pkt = []
    for abcid, mask in masks.items():
        clusters = _find_clusters(mask)
        for cl in clusters:
            # 6 bit ABCID, 11-bit cluster (lsb removed)
            word = bitarray(17)
            word[:6] = int2ba(abcid, length=6, endian='big')
            word[6:] = _resize_left(cl >> 1, 11)
            pkt.append(word)

    pkt[-1][0] = 1  # MSB=1 so emu packet gen knows when to stop reading
    return pkt


def regs_to_mask(regs: List[bitarray]) -> bitarray:
    """Converts 32-bit registers into 256-bit mask data as seen on chips.

    Returns
    -------
    bitarray
        256-bit strip masks (LSB is strip 0)
    """
    # loop left-to-right then flip mask after
    mask = bitarray(256)
    for i in range(16):
        for j in range(8):
            mask[i+(16*j)] = regs[j][2*i]
            mask[i+(16*j)+128] = regs[j][2*i+1]
    mask.reverse()
    return mask


def packet_from_regs(regs: Dict[int, List[bitarray]]) -> List[bitarray]:
    """Create partial HCC packet with ABC data from strip mask registers

    Emulates ABCStar chip data in Static Test mode, where strip masks are used
    as hits. The cluster words are formatted as they would be output by the
    receiving HCCStar (6-bit ABCID, 11-bit cluster data with LSB removed). This
    function invokes a call to `mask_from_regs()` to convert register values
    into strip masks, as the mapping is nontrivial.

    Parameters
    ----------
    masks : Dict[int, bitarray]
        ABCIDs and their corresponding 256-bit strip masks

    Returns
    -------
    List[bitarray]
        Packet words
    """
    masks = {abcid: regs_to_mask(regs[abcid]) for abcid in regs}
    return packet_from_masks(masks)


def save_packet(pkt: List[bitarray], fname: str) -> None:
    """Save packet to a text file

    Parameters
    ----------
    pkt : List[bitarray]
        Packet as generated by `packet_from_*()`
    fname : str
        Output filename

    """
    with open(fname, 'w') as fout:
        for word in pkt:
            fout.write(format(ba2int(word), "#07x")+'\n')


def load_packet(fname: str) -> List[bitarray]:
    """Load packet from a text file

    Parameters
    ----------
    fname : str
        Input filename

    Returns
    -------
    List[bitarray]
        Packet
    """
    pkt = []
    with open(fname, 'r') as fin:
        for line in fin:
            word = hex2ba(line.rstrip('\n')[2:], endian='big')
            pkt.append(_resize_left(word, 17))
    return pkt
