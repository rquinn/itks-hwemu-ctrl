"""example.py

This script provides example usage of the `datagen` and `memctl` modules.
Both modules expect data in the form of `bitarray`s.
"""

import memctl
import datagen
from bitarray.util import urandom

# memory block to talk to
flx_device = 2  # FELIX card 1 device 0
lpgbt_lane = 0
mem_id = (flx_device, lpgbt_lane)

# generate random mask registers for 10 ABCs
# registers are expected to be 32-bit bitarrays
n_abc = 10
abc_regs = {}
for abcid in range(n_abc):
    regs = [urandom(32)]
    abc_regs[abcid] = regs

# generate static test packet clusters
pkt = datagen.packet_from_regs(abc_regs)

# save packet to file
datagen.save_packet(pkt, 'data/random_regs.txt')

# load packet from file
pkt = datagen.load_packet('data/random_regs.txt')

# load packet into emulator memory
memctl.load(mem_id, pkt)

# can also directly instantiate mask and use that
# in this case, just a random assortment
# like registers, the mask is expected to be a 256-bit bitarray
abc_masks = {}
for abcid in range(n_abc):
    abc_masks[abcid] = urandom(256)
mask_pkt = datagen.packet_from_masks(abc_masks)

# create packet, reset memory, then load new packet
memctl.reset(mem_id)
memctl.load(mem_id, mask_pkt)
