"""defaults.py

Generate default data patterns, currently:
 - dizzy patterns up to 32 bits
 - inverted dizzy patterns
"""

import datagen
from bitarray.util import zeros

# dizzy patterns
for s in [1, 2, 4, 8, 16, 32]:
    mask = zeros(256)
    for i in range(0, 256, 2*s):
        mask[i:i+s] = 1
    mask[128:] = ~mask[128:]

    masks = {}
    for abcid in range(10):
        masks[abcid] = mask
    pkt = datagen.packet_from_masks(masks)
    datagen.save_packet(pkt, f'data/defaults/dizzy_{s}.txt')

    for abcid in range(10):
        masks[abcid] = ~mask
    pkt = datagen.packet_from_masks(masks)
    datagen.save_packet(pkt, f'data/defaults/dizzy_{s}_inverted.txt')
