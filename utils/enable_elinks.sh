#!/bin/bash
# 
# enable_elinks.sh
#
# This is a modified version of the phase-II elink enable scripts from
# https://gitlab.cern.ch/ezhivun/hcc-star-python-readout. In general,
# `links_enabled` can be modified to suit your setup, but `egroups_enabled`
# should always enable all 7 egroups.
#

links_enabled=(0 1 2 3)
egroups_enabled=(0 1 2 3 4 5 6)

device=0

enable_amac=0

for link in ${links_enabled[@]}
do
    link_fmt=$(printf "%02d" ${link})

    echo "configuring link ${link}:"

    echo -e "\tconfiguring AMAC elinks"
    flx-config -d ${device} set MINI_EGROUP_TOHOST_${link_fmt}_EC_ENCODING=0
    flx-config -d ${device} set MINI_EGROUP_FROMHOST_${link_fmt}_EC_ENCODING=0
    flx-config -d ${device} set MINI_EGROUP_TOHOST_${link_fmt}_EC_ENABLE=${enable_amac}
    flx-config -d ${device} set MINI_EGROUP_FROMHOST_${link_fmt}_EC_ENABLE=${enable_amac}
    flx-config -d ${device} set MINI_EGROUP_TOHOST_${link_fmt}_IC_ENABLE=${enable_amac}
    flx-config -d ${device} set MINI_EGROUP_FROMHOST_${link_fmt}_IC_ENABLE=${enable_amac}

    echo -e "\tenabling decoding elinks"
    for egroup in ${egroups_enabled[@]}
    do
        flx-config -d ${device} set DECODING_LINK${link_fmt}_EGROUP${egroup}_CTRL_EPATH_ENA=0xff
        flx-config -d ${device} set DECODING_LINK${link_fmt}_EGROUP${egroup}_CTRL_EPATH_WIDTH=0x02
    done

    echo -e "\tenabling encoding elinks"
    for egroup in 0 1 2 3
    do
        flx-config -d ${device} set ENCODING_LINK${link_fmt}_EGROUP${egroup}_CTRL_EPATH_ENA=0x00
        flx-config -d ${device} set ENCODING_LINK${link_fmt}_EGROUP${egroup}_CTRL_EPATH_ENA=0xff
    done
done

echo "configuring timeout"
flx-config -d ${device} set TIMEOUT_CTRL_ENABLE=1
flx-config -d ${device} set TIMEOUT_CTRL_TIMEOUT=0xffffffff